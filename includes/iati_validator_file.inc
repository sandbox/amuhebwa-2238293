<?php 
/**
 * @file.
 * @param  $fileurl. Url of the xml to validate
 * @return $result; an array with two elements; validity:-valid/invalid and an array of errors
 * for the invalid file
 */

function iati_validator_file_fileValidator($fileurl) {
  $dir = $fileurl; 
  $good_files = $invalid_files = array();
  $result = array();
  if ($dir) {
    // Enable user error handling
    libxml_use_internal_errors(true);
    $xml = new DOMDocument();
    $xml->load ($dir);
    $version = "";
    // Get the version of the Activity file
    $versionnodeactivities = $xml->getElementsByTagName("iati-activities");
    foreach ($versionnodeactivities as $versionnode) {
      $version = $versionnode->getAttribute("version");
    }
    // Get the version of the Organization file
    $versionnodeorganization = $xml->getElementsByTagName("iati-organisations");
    foreach ($versionnodeorganization as $versionnode) {
      $version = $versionnode->getAttribute("version");
    }
    // Based on the version, load the corresponding schema
    if ($version <= 1.01) { // version 1.00 /1.01
      if ($xml->getElementsByTagName("iati_organisation")->length == 0) {
        $xsd = str_replace("includes", "/schemas/schemas1_01/iati-activities-schema.xsd", dirname( __FILE__ ));
      }
      else {
        $xsd = str_replace("includes", "/schemas/schemas1_01/iati-organisations-schema.xsd", dirname( __FILE__ ));
      }
    }
    elseif ($version == 1.02) { //Version 1.02
      if ($xml->getElementsByTagName("iati_organisation")->length == 0) {
        $xsd = str_replace("includes", "/schemas/schemas1_02/iati-activities-schema.xsd", dirname( __FILE__ ));
      }
      else {
        $xsd = str_replace("includes", "/schemas/schemas1_02/iati-organisations-schema.xsd", dirname( __FILE__ ));
      }
    }
    else { // Version 1.03
      if ($xml->getElementsByTagName("iati_organisation")->length == 0) {
        $xsd = str_replace("includes", "/schemas/schemas1_02/iati-activities-schema.xsd", dirname( __FILE__ ));
      }
      else {
        $xsd = str_replace("includes", "/schemas/schemas1_03/iati-organisations-schema.xsd", dirname( __FILE__ ));
      }
    }
    if ($xml->schemaValidate($xsd)) {
      $result["validity"] = "valid";
      $result['errors'] = array("");
    }
    else {
      // $errors = _iati_validator_libxml_display_errors();
      //  print($errors);
      // drupal_set_message('<pre>'. print_r($errors, TRUE) .'</pre>');
      // drupal_set_message("Bad file");
      $result["validity"] = "invalid";
      $result['errors'] = _iati_validator_libxml_display_errors();
    }
  }
  else {
    // No file has been loaded. 
  }
  return $result;
}
/**
 * Checks which type of error has occured and return an array
 * with the error type, error and the line on which it exists
 * @param unknown $error
 * @return multitype:string NULL
 */

function _iati_validator_libxml_createerrorarray($error) {
  $return = "\n";
  $errobag = array();
  switch ($error->level) {
    case LIBXML_ERR_WARNING:
      $return .= "\n Warning $error->code : ";
      break;
    case LIBXML_ERR_ERROR:
      $return .= "\n Error $error->code : ";
      break;
    case LIBXML_ERR_FATAL:
      $return .= "\n Fatal Error $error->code : ";
      break;
  }
  $errobag['ERROR_CODE'] = $return;
  $errobag['ERROR_MESSAGE'] = trim($error->message);
  $errobag['ERROR_LINE'] = $error->line;
  return $errobag;
}

/**
 * This will print each error type, with corresponding error and the lines on which it exists
 * @return $arrayoerrors. An array of errors
 */
function _iati_validator_libxml_display_errors() {
  $errors = libxml_get_errors();
  $arrayoerrors = array();
  foreach ($errors as $error) {
    $errorset = _iati_validator_libxml_createerrorarray($error);
    $code = $errorset["ERROR_CODE"];
    $message = $errorset["ERROR_MESSAGE"];
    $line = $errorset["ERROR_LINE"];
    $arrayoerrors[$code]['title'] = $message;
    $arrayoerrors[$code]['lines'][] = $line;
  }
  libxml_clear_errors();
  return $arrayoerrors;
}
